import json
import os

import pandas as pd
import numpy as np

from django.shortcuts import render, render_to_response
from django.conf import settings

from main.models import Gene_Experiment, Experiment, Marker, Chromosome, GeneInfo
from main.utils import load_traitlist_for_experiment, load_lodscores_for_experiment

import multiplot


# Create your views here.

def eQTLPlotView(request):
    '''
    plot eQTL maping
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES).filter(gene_experiment__isnull=False).distinct()
    if request.method == 'GET' and request.GET.get('experiment_name'):
            exp_name = request.GET.get('experiment_name')
            
            experiment = Experiment.objects.get(name=exp_name)

            lodthld = experiment.lodthld

            thld = float(lodthld)

            if request.GET.get('thld'):
                try:
                    thld = float(request.GET.get('thld'))
                except ValueError:
                    thld = lodthld

            thld = int(thld * 10) / 10.0
            output = 'lod%.1f.json' % thld
            full_path = os.path.join(settings.MEDIA_ROOT, 'data', '%s' % exp_name, output)
            if not os.path.isfile(full_path) or os.stat(full_path).st_size == 0:
                outputJson(exp_name, thld, output)

            return render_to_response('cistrans.html', {'experiment_name': exp_name,
                                                    'thld': thld,
                                                    'lodthld': lodthld,
                                                    'current_experiment': experiment,
                                                    'experiments': experiments})
    else:
        return render_to_response('cistrans.html', {'experiments': experiments})


def getMarkers(experiment):
    marker_list = Marker.objects.filter(expierment__experiment_name=experiment).order_by('chromosome',
                                                                                         'start').values_list('name',
                                                                                                              flat=True)
    return marker_list


def getChrMarkers(experiment, i):
    marker_list = Marker.objects.filter(experiment__name=experiment, chromosome__name=i).order_by(
        'start').values_list('name', flat=True)
    encode_marker_list = [marker.encode('ascii', 'ignore') for marker in marker_list]
    return encode_marker_list


def getMarkerObjects(experiment):
    marker_list = Marker.objects.filter(experiment__name=experiment).order_by('chromosome', 'start')
    return marker_list

def getSpecies(exp):
    species_name = Experiment.objects.get(name=exp).species.species_name
    return species_name


def getMarkerPos(exp, marker):
    return Marker.objects.get(experiment__name=exp, name=marker).start


def getMarkerNames(experiment_name):
    return Marker.objects.filter(experiment__name=experiment_name).values_list("name", flat=True)


def ranges(series_list):
    '''
    identify groups of continuous numbers in a list and group the continuous numbers as a sub-list
    for example,
    [1 ,2 ,6, 7, 8, 9, 10, 11]
    [(1,2),(6, 11)]
    '''
    start, end = series_list[0], series_list[0]
    for n in series_list[1:]:
        if n - 1 == end:  # Part of the group, bump the end
            end = n
        else:  # Not part of the group, yield current group and start a new
            yield start, end
            start = end = n
    yield start, end  # Yield the last group


def outputJson(experiment_name, thld, output):
    '''
    description: parse upload information into JSON file format later which later will be used for multiexperimentplot plot.

    @type experiment_name: name of experiment
    @param experiment_name: string

    @type thld: LOD theshold
    @param thld: decimal

    @rtype output: path of output file
    @return output: string

    '''

    out_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name, output))

    output_dic = {}

    # define KEY chrnames and chr
    species_name = getSpecies(experiment_name)

    chromosomes = Chromosome.objects.filter(species__species_name=species_name)
    output_dic['chrnames'] = sorted(list(chromosomes.values_list("name", flat=True)))
    chr_dic = dict()
    for chromosome in chromosomes:
        chr_dic[chromosome.name] = {"start": chromosome.start, "end": chromosome.end}
    output_dic['chr'] = chr_dic
    chr_nr = output_dic['chrnames']

    genotype_file = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name, "map.txt"))
    if os.path.exists(genotype_file):
        genotypes = pd.read_csv(genotype_file,sep="\t")

        gd = genotypes.T.to_dict('list')

        output_dic['geno'] = gd
        output_dic['individuals'] = list(genotypes.columns)

    # define KEY spec
    output_dic['spec'] = species_name;

    # define KEY pmarknames
    chr_marker_list_dic = {}
    for i in chr_nr:
        chr_marker_list_dic[i] = list(getChrMarkers(experiment_name, i))  # django.db.models.query.ValuesListQuerySet
    output_dic['pmarknames'] = chr_marker_list_dic

    # define KEY markers
    markers = list(getMarkerNames(experiment_name))
    output_dic['markers'] = markers

    experiment = Experiment.objects.get(name = experiment_name)
    output_dic['experiment_name'] = experiment.name
    output_dic['parental_strain'] = experiment.parental_strain
    output_dic['sample'] = experiment.sample
    output_dic['pub_url'] = experiment.pub_url
    output_dic['reference'] = experiment.reference
    output_dic['lodthld'] = "%.2f"%experiment.lodthld

    # define KEY pmark
    marker_list_dic = {}
    marker_queryset_list = getMarkerObjects(experiment_name)
    for m in marker_queryset_list:
        m_info = {'chr': m.chromosome.name, 'start': int(m.start)}
        marker_list_dic[m.name.encode('ascii', 'ignore')] = m_info
    output_dic['pmark'] = marker_list_dic

    genes_in_experiment = {}
    gene_list = Gene_Experiment.objects.filter(experiment__name = experiment_name)
    for gene in gene_list:
       genes_in_experiment[gene.gene_id] = gene

    # define KEY spot/gene
    genes_dic = {}

    i = 0
    peaks_list = []

    chr_names = output_dic['chrnames']
    # count number of markers per chromosome
    chr_nr_of_markers = {}

    for chr_ref1 in chr_names:
        chr_nr_of_markers[chr_ref1] = len(output_dic['pmarknames'][chr_ref1])

    genes = load_traitlist_for_experiment(experiment_name)
    lod_scores = load_lodscores_for_experiment(experiment_name)
    for iGene in range(0,len(genes)):
        gene_id = genes[iGene].upper()
        #check if the transcript/gene id exists in the database
        if gene_id not in genes_in_experiment:
            print gene_id
            continue
        # assign LOD value to 0 if null
        lod_list = lod_scores[iGene,:]
        k = 0  # splice index
        lod_list_chr = {}  # initialise lod_list_chr
        # splice lod_list to sub_lod_list per chromosome
        for chr_ref2 in chr_names:
            lod_list_chr[chr_ref2] = lod_list[k:k + chr_nr_of_markers[chr_ref2]]
            k += chr_nr_of_markers[chr_ref2]

        for chr_ref3 in chr_names:  # lod list per chromosome
            # check if any significant LOD value in the list per chromosome
            if any(x for x in lod_list_chr[chr_ref3] if abs(x) >= thld):
                ref_index_list = []
                for m in range(len(lod_list_chr[chr_ref3])):
                    if abs(lod_list_chr[chr_ref3][m]) >= thld:
                        ref_index_list.append(m)
                # group continuous significant LOD
                interval_list = list(ranges(ref_index_list))

                # if any intervals
                for interval in interval_list:
                    i += 1
                    start, end = interval[0], interval[1]

                    # LOD peak and LOD index
                    # find the max LOD value per significant LOD interval per chromosome
                    qtl_lod = max(lod_list_chr[chr_ref3][start:end + 1])
                    qtl_index = np.where(lod_list_chr[chr_ref3] == qtl_lod)[0][0]  # LOD peak

                    # initialize eQTL interval
                    inv_start = inv_start_lod_support = output_dic['chr'][chr_ref3]['start']
                    inv_end = inv_end_lod_support = output_dic['chr'][chr_ref3]['end']

                    # 1 LOD support interval
                    lod_support = qtl_lod - 1

                    ##initialize eQTL interval
                    inv_start_ind_lod_support = inv_end_ind_lod_support = qtl_index

                    for ind in range(qtl_index, start - 1 if start != 0 else start):
                        if lod_list_chr[chr_ref3][ind] > lod_support:
                            inv_start_ind_lod_support = ind
                    for ind in range(qtl_index, end + 1):  # end+1 will not raise list index exception
                        if lod_list_chr[chr_ref3][ind] > lod_support:
                            inv_end_ind_lod_support = ind

                    # define LOD interval
                    if start > 0 and end < len(lod_list_chr[chr_ref3]) - 1:
                        marker_name = output_dic['pmarknames'][chr_ref3][start - 1]
                        inv_start = marker_list_dic[marker_name]["start"]

                        marker_name = output_dic['pmarknames'][chr_ref3][end + 1]
                        inv_end = marker_list_dic[marker_name]["start"]

                    if start == 0 and end < len(lod_list_chr[chr_ref3]) - 1:
                        marker_name = output_dic['pmarknames'][chr_ref3][end + 1]
                        inv_end = marker_list_dic[marker_name]["start"]

                    if start > 0 and end == len(lod_list_chr[chr_ref3]) - 1:
                        marker_name = output_dic['pmarknames'][chr_ref3][start - 1]
                        inv_start = marker_list_dic[marker_name]["start"]

                    # define one LOD support interval
                    if inv_start_ind_lod_support == 0 and inv_end_ind_lod_support != len(
                            lod_list_chr[chr_ref3]) - 1:
                        marker_name = output_dic['pmarknames'][chr_ref3][inv_end_ind_lod_support + 1]
                        inv_end_lod_support = marker_list_dic[marker_name]["start"]
                    if inv_start_ind_lod_support != 0 and inv_end_ind_lod_support == len(
                            lod_list_chr[chr_ref3]) - 1:
                        marker_name = output_dic['pmarknames'][chr_ref3][inv_start_ind_lod_support - 1]
                        inv_start_lod_support = marker_list_dic[marker_name]["start"]
                    if inv_start_ind_lod_support != 0 and inv_end_ind_lod_support != len(
                            lod_list_chr[chr_ref3]) - 1:
                        marker_name = output_dic['pmarknames'][chr_ref3][inv_start_ind_lod_support - 1]
                        inv_start_lod_support = marker_list_dic[marker_name]["start"]

                        marker_name = output_dic['pmarknames'][chr_ref3][inv_end_ind_lod_support + 1]
                        inv_end_lod_support = marker_list_dic[marker_name]["start"]

                    # plot only peak LOD
                    lod_dic = {}  # empty dictionary for each iteration

                    # save to lod_dic
                    lod_dic['interval'] = '%s:%s-%s' % (chr_ref3, inv_start, inv_end)
                    lod_dic['lod_support_interval'] = '%s:%s-%s' % (
                    chr_ref3, inv_start_lod_support, inv_end_lod_support)
                    lod_dic['spot'] = gene_id
                    lod_dic['marker'] = output_dic['pmarknames'][chr_ref3][qtl_index]
                    lod_dic['lod'] = lod_list_chr[chr_ref3][qtl_index]
                    lod_dic['gene_name'] = genes_in_experiment[gene_id].geneinfo.gene_name
                    genes_dic[gene_id] = 1
                    peaks_list.append(lod_dic)

    output_dic['peaks'] = peaks_list
    for gene in genes_dic:
        geneInfo = GeneInfo.objects.get(gene_id=gene)
        if geneInfo.start and geneInfo.chr:
            genes_dic[gene] = {'chr': geneInfo.chr, 'start': int(geneInfo.start), 'end': int(geneInfo.end),
                                       'gene_name': geneInfo.gene_name}
        else:
            genes_dic[gene] = {'chr': 'I', 'start': 0, 'end': 0, 'gene_name': geneInfo.gene_name}
    output_dic['spot'] = genes_dic

    with open(out_path, 'w') as fo:
        json.dump(output_dic, fo, indent=4)

