from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.conf import settings

from main.models import Experiment, Gene_Experiment

import csv

def index(request):
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)

    mode = 'landing'

    if request.method == 'GET':
        if request.GET.get('mode'):
            mode = request.GET.get('mode')

    return render_to_response('index.html',{'experiments':experiments,
                                            'mode' :mode})

def no_results(query, mode):
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)

    return render_to_response('index.html', {'experiments': experiments,
                                             'query': query,
                                             'noresults': 1,
                                             'mode': mode})


def AGI_list(request):
    genelist = Gene_Experiment.objects.filter(experiment__species__species_name=settings.SPECIES).values_list('gene_id', flat=True)
    AGI_list = '\n'.join(genelist)

    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename="ELEQTL_ID_list.txt"'
    response.write(AGI_list)

    return response


