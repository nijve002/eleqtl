from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^AGI_list$', views.AGI_list, name='AGI_list'),
]
