# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Species(models.Model):
    species_name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=45)
    url = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'main_species'


class Chromosome(models.Model):
    name = models.CharField(max_length=50)
    start = models.IntegerField()
    end = models.IntegerField()
    species = models.ForeignKey(Species)
	
    class Meta:
        managed = False
        db_table = 'main_chromosome'


class Experiment(models.Model):
    species = models.ForeignKey(Species)
    name = models.CharField(max_length=50)
    phenotypes = models.CharField(max_length=50)
    type_of_array = models.CharField(max_length=50)
    sample_size = models.CharField(max_length=50)
    parental_strain = models.CharField(max_length=50)
    sample = models.CharField(max_length=100)
    reference = models.CharField(max_length=200)
    pub_url = models.URLField(max_length=300)
    pub_date = models.DateTimeField()
    upload_user_id = models.IntegerField()
    array_file = models.CharField(max_length=100)
    marker_file = models.CharField(max_length=100)
    genotype_file = models.CharField(max_length=100)
    lod_file = models.CharField(max_length=100)
    pvalthld = models.DecimalField(max_digits=13, decimal_places=12)
    lodthld = models.DecimalField(max_digits=6, decimal_places=3)

    class Meta:
        managed = False
        db_table = 'main_experiment'


class Line(models.Model):
    experiment = models.ForeignKey(Experiment)
    line_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'main_line'


class Marker(models.Model):
    name = models.CharField(max_length=40)
    chromosome = models.ForeignKey(Chromosome)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)
    experiment = models.ForeignKey(Experiment)

    class Meta:
        managed = False
        db_table = 'main_marker'
        
class Marker_Experiment(models.Model):
    experiment = models.ForeignKey(Experiment)
    marker = models.ForeignKey(Marker)

    class Meta:
        managed = False
        db_table = 'main_marker_experiment'

class GeneInfo(models.Model):
    species = models.ForeignKey(Species) 
    gene_id = models.CharField(max_length=30, blank=True, null=True, unique=True)
    description = models.TextField(blank=True,null=True)
    gene_name = models.CharField(max_length=30, blank=True, null=True, unique=True)
    chr = models.CharField(max_length=3)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_geneinfo'

class Transcript(models.Model): 
    geneinfo = models.ForeignKey(GeneInfo)
    transcript_id = models.CharField(max_length=20, blank=True, null=True)
    chr = models.CharField(max_length=3)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_transcript'


class GO(models.Model):
    accession = models.CharField(primary_key=True,max_length=30,blank=False,null=False, unique=True)
    name = models.CharField(max_length=255,blank=False,null=False)
    definition = models.TextField()
    domain = models.CharField(max_length=255,blank=False,null=False)

    class Meta:
        managed = False
        db_table = 'main_go'

class GeneGO(models.Model):
    geneinfo = models.ForeignKey(GeneInfo)
    term_accession = models.ForeignKey(GO,to_field='accession', db_column = 'term_accession')
    term_evidence_code = models.CharField(max_length=30,blank=False,null=False)

    class Meta:
        managed = False
        db_table = 'main_gene_go'

class Gene_Experiment(models.Model):
    experiment = models.ForeignKey(Experiment)
    geneinfo = models.ForeignKey(GeneInfo)
    gene_id = models.CharField(max_length=50)
    max_lod_score = models.FloatField()

    class Meta:
        managed = False
        db_table = 'main_gene_experiment'

class Phenotype(models.Model):
    phenotype_id = models.CharField(max_length=255, blank=False, null=False, unique=True)
    name = models.CharField(max_length=255,blank=False,null=False)
    description = models.TextField(blank=True,null=True)
    phenotype = models.TextField(blank=True,null=True)
    treatment = models.TextField(blank=True,null=True)
    trait = models.TextField(blank=True,null=True)
    statistic = models.TextField(blank=True,null=True)
    stage = models.TextField(blank=True,null=True)
    population = models.TextField(blank=True,null=True)
    unit = models.TextField(blank=True,null=True)
    comment = models.TextField(blank=True,null=True)
    GO_term = models.TextField(blank=True,null=True)

    class Meta:
        managed = False
        db_table = 'main_phenotype'

class Phenotype_Experiment(models.Model):
    experiment = models.ForeignKey(Experiment)
    phenotype = models.ForeignKey(Phenotype)
    max_lod_score = models.FloatField()

    class Meta:
        managed = False
        db_table = 'main_phenotype_experiment'
