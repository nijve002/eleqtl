import simplejson as json
import os
import shutil
import datetime
from django.db import transaction
import urllib
import sys
from main.models import Experiment, GeneGO,Gene_Experiment,Transcript,Marker,\
            Species, Chromosome, GeneInfo, Phenotype, GO,Phenotype_Experiment
            
from math import factorial
from django.conf import settings
from scipy import stats
from statsmodels.sandbox.stats.multicomp import multipletests

GENE_LOD_FILE = "lod.txt"
PHENOTYPE_LOD_FILE = "lod_phenotypes.txt"


import numpy as np
import pandas as pd

def GO_enrichment(gene_set, exp_name, alpha=0.05): 
    gene_set_count = len(gene_set)

    if gene_set_count < 10:
        return {}

    genes = GeneInfo.objects.filter(gene_experiment__experiment__name = exp_name)
    gene_go = genes.values_list("gene_id","genego__term_accession")
    gene_count = genes.distinct().count()
        
    gene_GO = {}
    GO_gene = {}

    for gene, go_term in gene_go:
        if gene not in gene_GO: 
            gene_GO[gene] = [] 
        if go_term not in GO_gene: 
            GO_gene[go_term] = []
        gene_GO[gene].append(go_term) 
        GO_gene[go_term].append(gene)
        
    GO_gene_set = {}
    for gene in gene_set:
        if gene not in gene_GO :
            continue
        for go_term in gene_GO[gene]:
            if go_term not in GO_gene_set:
                GO_gene_set[go_term] = []
            GO_gene_set[go_term].append(gene)

    if len(GO_gene_set) == 0:
        return {}

    # only consider GO terms that occur at least in 0.1% of the genes and at most in 25% of the genes
    min_count = 0.001 * gene_count
    max_count = 0.25 * gene_count

    go_term_pvalues = {}

    for go_term in GO_gene_set:
        overall_go_count = len(GO_gene[go_term])
        set_go_count = len(GO_gene_set[go_term])
        
        if overall_go_count < min_count or overall_go_count > max_count:
            continue
        
        a = set_go_count
        b = overall_go_count - a
        c = gene_set_count - a
        d = gene_count - a - b - c
        oddsratio, pvalue = stats.fisher_exact([[a, b], [c, d]])
        go_term_pvalues[go_term] = pvalue
        
    go_terms = go_term_pvalues.keys()
    pvalues = go_term_pvalues.values()
        
    (reject,qvalues,alphacSidak,alphacBonf) = multipletests(pvalues, 0.01, method='fdr_bh')
    corrected_pvalues = dict(zip(go_terms,qvalues))

    filtered_go_terms = {go_term:qvalue for (go_term,qvalue) in corrected_pvalues.items() if qvalue < alpha }

    result = {}
    go_objects = GO.objects.filter(accession__in=filtered_go_terms.keys())
    for go in go_objects:
        result[go.accession] = [go,filtered_go_terms[go.accession]]
    return result

# noinspection PyInterpreter
def load_traitlist_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    traits_pickle_path = os.path.join(experiment_data_path, '%s' % "traits.npy")
    if not os.path.exists(traits_pickle_path):
        create_pickles_for_experiment(experiment_name)
    traits = np.load(traits_pickle_path)
    return traits


def load_markerlist_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")
    if not os.path.exists(markers_pickle_path):
        create_pickles_for_experiment(experiment_name)
    markers = np.load(markers_pickle_path)
    return markers


def load_lodscores_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    if not os.path.exists(lod_pickle_path):
        create_pickles_for_experiment(experiment_name)
    lod_scores = np.load(lod_pickle_path)
    return lod_scores


def create_pickles_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    traits_pickle_path = os.path.join(experiment_data_path, '%s' % "traits.npy")
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")

    lod_data = None

    lod_file = Experiment.objects.get(name=experiment_name).lod_file
    lod_file_path = os.path.join(experiment_data_path, '%s' % lod_file)
    if os.path.exists(lod_file_path):
        lod_data = pd.read_csv(lod_file_path, sep='\t',index_col=0).fillna(0)
        lod_data.drop([None], errors='ignore', inplace=True) # remove rows with no gene name


    phenotype_lod_file_path = os.path.join(experiment_data_path, '%s' % PHENOTYPE_LOD_FILE)
    if os.path.exists(phenotype_lod_file_path):
        t_data = pd.read_csv(phenotype_lod_file_path, sep='\t',index_col=0).fillna(0)
        t_data.index = t_data.index.str.strip()
        
        if lod_data is None:
            lod_data = t_data
        else:
            lod_data = lod_data.append(t_data)

    if lod_data is None:
        print "no data for experiment %s"%(experiment_name)
        return

    lod_data.columns = [c.strip() for c in lod_data.columns.values]
    lod_data.index = lod_data.index.str.upper()

    markers = lod_data.columns.values
    traits = lod_data.index.values
    data = np.asarray(lod_data).astype(np.float)

    np.save(markers_pickle_path, markers)
    np.save(lod_pickle_path, data)
    np.save(traits_pickle_path, traits)

def update_max_lod_in_database():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name",flat=True):
        print("updating max_lod for experiment: %s"%experiment_name)
        update_max_lod_for_experiment(experiment_name)
        
def update_max_lod_for_experiment(experiment_name):
    traits = load_traitlist_for_experiment(experiment_name)
    lod_scores = load_lodscores_for_experiment(experiment_name)
    max_lod_trait = np.amax(np.fabs(lod_scores),axis=1)
    trait2max =dict(zip(traits,max_lod_trait))
    
    genes_for_experiment = Gene_Experiment.objects.filter(experiment__name=experiment_name)
    
    with transaction.atomic():
        for gene in genes_for_experiment:
            if gene.gene_id in trait2max:
                gene.max_lod_score = trait2max[gene.gene_id]
            else:
                gene.max_lod_score = 0
            gene.save()

        phenotypes_for_experiment = Phenotype_Experiment.objects.filter(experiment__name = experiment_name)
        for phenotype in phenotypes_for_experiment:
            if phenotype.phenotype.phenotype_id in trait2max:
                phenotype.max_lod_score = trait2max[phenotype.phenotype.phenotype_id]
            else:
                phenotype.max_lod_score = 0
            phenotype.save()

                
def create_pickles():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name",flat=True):
        create_pickles_for_experiment(experiment_name)


def get_markers_on_chr(experiment_name, chromosome):
    if len(Chromosome.objects.filter(name=chromosome,species__species_name=settings.SPECIES)) == 0:
        try:
            chromosome = int(chromosome)
            if chromosome > 0 and chromosome < 10:
                roman = ["I","II","III","IV","V","VI","VII","VIII","IX"]
                chromosome = roman[chromosome - 1]
        except ValueError:
            pass
    return Marker.objects.filter(experiment__name = experiment_name, chromosome__name = chromosome)

def get_closest_marker(experiment_name, chromosome, pos):
    pos = int(pos)
    markers = get_markers_on_chr(experiment_name, chromosome)
    minDist = -1
    closestMarker = None
    for marker in markers:
        dist = abs(pos - marker.start)
        if minDist == -1 or dist < minDist:
            minDist = dist
            closestMarker = marker

    return closestMarker

def get_neighboring_markers(experiment_name, marker_name):
    marker = Marker.objects.get(experiment__name = experiment_name, 
        name = marker_name)
    
    markers = Marker.objects.filter(experiment__name = experiment_name, 
        chromosome = marker.chromosome)
    
    before = None
    after = None
    for m in markers:
        if m.start < marker.start:
            if not before or before.start < m.start:
                before = m
        elif m.start > marker.start:
            if not after or after.start > m.start:
                after = m
    return (before,after)
                

def get_phenotype(query, experiment_name = None):
    phenotypes = Phenotype.objects.filter(phenotype_id__iexact=query)
    if experiment_name:
        phenotypes = phenotypes.filter(phenotype_experiment__experiment__name = experiment_name)

        if phenotypes.count() == 1:
            return phenotypes[0]
        else:
            return None

    if phenotypes.count() == 1:
        return phenotypes[0]
    return None


def get_gene(query, experiment_name = None):
    genes = GeneInfo.objects.filter(gene_id__iexact = query)
    if experiment_name:
        genes = genes.filter(gene_experiment__experiment__name = experiment_name)

    if genes.count() == 1:
        return genes[0]

    genes = GeneInfo.objects.filter(gene_name__iexact=query)
    if experiment_name:
        genes = genes.filter(gene_experiment__experiment__name = experiment_name)

    if genes.count() == 1:
        return genes[0]

    return None

def get_marker_objects(experiment_name):
    return Marker.objects.filter(experiment__name = experiment_name)


def get_species_for_experiment(experiment_name):
    return Species.objects.get(experiment__name = experiment_name).species_name


def get_marker_names(experiment_name):
    return Marker.objects.filter(experiment__name = experiment_name).values_list("name", flat=True)


def create_settings_json_for_experiment(experiment_name):
    output_dic = dict()

    species_name = get_species_for_experiment(experiment_name)
    output_dic['species'] = species_name
    
    experiment = Experiment.objects.get(name=experiment_name)
    output_dic['lodthld'] = experiment.lodthld

    url = Species.objects.filter(species_name=species_name).values_list('url', flat=True)
    if len(url) == 1:
        output_dic['url'] = url[0]
    else:
        output_dic['url'] = ""

    # only report chromosomes for with markers are available
    chromosomes = Chromosome.objects.filter(marker__experiment__name=experiment_name).distinct()
    output_dic['chrnames'] = sorted(chromosomes.values_list('name', flat=True))

    chr_dic = dict()
    for chromosome in chromosomes:
        chr_dic[chromosome.name] = {"start": chromosome.start, "end": chromosome.end}

    output_dic["chr"] = chr_dic

    out_file = os.path.join(settings.MEDIA_ROOT,'data/%s/%s.json' % (experiment_name,"settings"))

    with open(out_file, 'w') as fo:
        json.dump(output_dic, fo, indent=4, ensure_ascii=True)


def create_trait_json_for_all():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name",flat=True):
        create_trait_json_for_experiment(experiment_name)


def create_trait_json_for_experiment(experiment_name):
    '''
    description: parse upload information into JSON file format later which later will be used for multiexperimentplot plot. 

    @type experiment_name: name of experiment
    @param experiment_name: string
    
    @type thld: LOD theshold
    @param thld: decimal

    @rtype output: path of output file
    @return output: string
    
    '''

    species_name = get_species_for_experiment(experiment_name)
    geneInfo_list = list(GeneInfo.objects.filter(species__species_name=species_name).values('gene_id','gene_name','chr','start','end'))
    geneInfo_dict = {g["gene_id"]:g for g in geneInfo_list}

    phenotype_list = list(Phenotype.objects.filter(phenotype_experiment__experiment__name=experiment_name).values('phenotype_id','name','description'))
    phenotype_dict = {p["phenotype_id"]:p for p in phenotype_list}
    
    marker_list = list(Marker.objects.filter(experiment__name=experiment_name).values('name','chromosome__name','start','end'))
    marker_dict = {m["name"]:m for m in marker_list}

    traits_directory = os.path.join(settings.MEDIA_ROOT, 'data/%s/traits/' % (experiment_name))
    if not os.path.exists(traits_directory):
        print "making %s"%(traits_directory)
        os.makedirs(traits_directory)
        
    for trait_file in os.listdir(traits_directory):
        trait_file_path = os.path.join(traits_directory, trait_file)
        if os.path.isfile(trait_file_path):
            os.unlink(trait_file_path)

    probes_directory = os.path.join(settings.MEDIA_ROOT, 'data/%s/probe/' % (experiment_name))
    if not os.path.exists(probes_directory):
        print "making %s"%(probes_directory)
        os.makedirs(probes_directory)
        
    for probe_file in os.listdir(probes_directory):
        probe_file_path = os.path.join(probes_directory, probe_file)
        if os.path.isfile(probe_file_path):
            os.unlink(probe_file_path)

    markers = load_markerlist_for_experiment(experiment_name)
    for markerName in markers:
        if markerName not in marker_dict:
            sys.stderr.write("marker not in database: %s\n" % markerName)
            return()

    measurements = None
    measurement_file = os.path.join(settings.MEDIA_ROOT, 'data/%s/phenotypes.txt' % experiment_name)
    if os.path.exists(measurement_file):
        measurements = pd.read_csv(measurement_file, sep='\t',index_col=0)
        measurements.index = measurements.index.str.upper()
        
    lod_scores = load_lodscores_for_experiment(experiment_name)
    traits = load_traitlist_for_experiment(experiment_name)
    for i_trait, trait in enumerate(traits):
        trait = trait.upper().replace(' ','_')
        
        output_dic = {}
        output_dic["experiment"] = experiment_name
        
        output_dic["ID"] = trait
        if trait in geneInfo_dict:
            geneInfo = geneInfo_dict[trait]
            output_dic["type"] = "gene"
            output_dic["name"] = geneInfo["gene_name"]
            output_dic["chr"] = geneInfo["chr"]
            output_dic["start"] = geneInfo["start"]
            output_dic["end"] = geneInfo["end"]
        elif trait in phenotype_dict:
            output_dic["type"] = "phenotype"
            output_dic["name"] = phenotype_dict[trait]["name"]
            output_dic["Description"] = phenotype_dict[trait]["description"]
        else:
            sys.stderr.write("trait not in database: %s, (species_name: %s)\n"%(trait, species_name))
            continue
            
        lodscore_dict = {}

        for i_marker, markerName in enumerate(markers):
            marker = marker_dict[markerName]
            lodscore_dict[markerName] = dict(chr=marker["chromosome__name"],
                    start=marker["start"], end=marker["end"], lod=lod_scores[i_trait, i_marker])

        output_dic["lodscores"] = lodscore_dict

        out_file = os.path.join(settings.MEDIA_ROOT, 'data/%s/traits/%s.json' % (experiment_name, trait))

        with open(out_file, 'w') as fo:
            json.dump(output_dic, fo)

        if output_dic["type"] is not "gene":
            continue
            
        probe_output_dic = dict()
        probe_output_dic["probe"] = trait
        probe_output_dic["lod"] = list(lod_scores[i_trait,:])
        if measurements is not None:
            probe_output_dic["pheno"] = list(measurements.loc[trait])
        probe_out_file = os.path.join(settings.MEDIA_ROOT, 'data/%s/probe/%s.json' % (experiment_name, trait))

        with open(probe_out_file, 'w') as fo:
            json.dump(probe_output_dic, fo)


def check_for_new_experiment():
    data_path = os.path.join(settings.MEDIA_ROOT, 'data/')
    experiment_dirs = os.listdir(data_path)
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name", flat=True)
    for experiment_dir in experiment_dirs:
        if os.path.isdir(os.path.join(data_path,experiment_dir)) and \
            experiment_dir[0] != "." and \
            experiment_dir not in experiments:
                print("creating experiment: %s"%experiment_dir)
                create_experiment(experiment_dir)

def clean_deleted_experiments():
    data_path = os.path.join(settings.MEDIA_ROOT, 'data/')
    experiment_dirs = os.listdir(data_path)
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name", flat=True)
    for experiment in experiments:
        if experiment not in experiment_dirs:
            print("removing experiment: %s"%experiment)
            delete_experiment(experiment)

def load_marker_file_in_database(experiment_name):
    Marker.objects.filter(experiment__name=experiment_name).delete()
    experiment = Experiment.objects.get(name=experiment_name)
    marker_file_name = experiment.marker_file
    marker_file_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name,marker_file_name))
    with open(marker_file_path) as marker_file:
        header = marker_file.readline().split()
        for line in marker_file:
            fields = line.rstrip().split()
            marker = Marker()
            
            if len(fields) == len(header):
                marker.name = fields[0]
                marker.start = "%d"%int(float(fields[2]))
                marker.end = "%d"%int(float(fields[3]))
                chromosome = Chromosome.objects.get(name=fields[1],species__species_name=settings.SPECIES)
            else:            
                marker.name = fields[1]
                marker.start = "%d"%int(float(fields[3]))
                marker.end = "%d"%int(float(fields[3]))
                chromosome = Chromosome.objects.get(name=fields[2],species__species_name=settings.SPECIES)
            
            marker.chromosome = chromosome
            marker.experiment = experiment
            try:
                marker.save()
            except ValueError as e:
                sys.stderr.write("cannot import marker line: %s (%s)")%(line,e.strerror)


def add_genes_for_experiment(experiment_name):
    Gene_Experiment.objects.filter(experiment__name = experiment_name).delete()
    experiment = Experiment.objects.get(name=experiment_name)
    species = Species.objects.get(species_name=settings.SPECIES)

    lod_file_name = experiment.lod_file
    lod_file_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name, lod_file_name))
    with open(lod_file_path) as lod_file:
        header = lod_file.readline()
        for line in lod_file:
            fields = line.rstrip().split()
            gene = fields[0].upper()

            gis = GeneInfo.objects.filter(gene_id=gene)
            if len(gis) == 0:
                gi = GeneInfo()
                gi.gene_id = gene
                gi.species = species
                gi.save()

            gene_info = GeneInfo.objects.get(gene_id=gene)

            gene_experiments = Gene_Experiment.objects.filter(experiment__name = experiment_name).filter(gene_id = gene)
            if len(gene_experiments) == 0:
                gene_experiment = Gene_Experiment()
                gene_experiment.experiment = experiment
                gene_experiment.gene_id = gene
                gene_experiment.geneinfo = gene_info
                gene_experiment.save()



def add_phenotypes_for_experiment(experiment_name):
    Phenotype_Experiment.objects.filter(experiment__name = experiment_name).delete()
    experiment = Experiment.objects.get(name=experiment_name)
    
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    data_description_file_path = os.path.join(experiment_data_path, '%s' % 'data_description.txt')
    
    data_description = pd.read_csv(data_description_file_path,sep='\t')
    
    for i,phenotype in data_description.iterrows():
        phenotype_id = "{}_{}".format(experiment_name,phenotype["Connective_trait"]).upper()
        p = Phenotype.objects.get_or_create(phenotype_id=phenotype_id)[0]
        p.name = phenotype["trait"]
        p.description = phenotype["trait"]
        p.phenotype = phenotype["Phenotype"]
        p.treatment = phenotype["treatment"]
        p.trait = phenotype["trait"]
        p.statistic = phenotype["statistic"]
        p.stage = phenotype["Stage"]
        p.population = phenotype["Population"]
        p.unit = phenotype["unit"]
        p.comment = phenotype["Comment"]
        p.GO_term = phenotype["GO.term"]
        p.save()
        Phenotype_Experiment.objects.create(phenotype=p,experiment=experiment)


def create_experiment(experiment_name):
    experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (experiment_name))
    if not os.path.exists(experiment_dir):
        sys.stderr.write("Cannot create experiment, directory does not exist: %s\n" % experiment_dir)
        return

    species = Species.objects.get(species_name=settings.SPECIES)
    Experiment.objects.filter(name=experiment_name).delete()
    experiment = Experiment()
    experiment.name = experiment_name
    experiment.species = species
    experiment.marker_file = "marker.txt"
    experiment.array_file = "array.txt"
    experiment.lod_file = "lod.txt"
    experiment.pub_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    experiment.upload_user_id = 1
    experiment.pvalthld = 0.01
    experiment.lodthld = 3
    experiment.save()

    load_marker_file_in_database(experiment_name)
    
    if os.path.exists(experiment_dir + "/%s"%GENE_LOD_FILE):
        add_genes_for_experiment(experiment_name)
    if os.path.exists(experiment_dir + "/%s"%PHENOTYPE_LOD_FILE):    
        add_phenotypes_for_experiment(experiment_name)
        
    create_settings_json_for_experiment(experiment_name)
    create_trait_json_for_experiment(experiment_name)
    create_pickles_for_experiment(experiment_name)


def delete_experiment(experiment_name):
    Gene_Experiment.objects.filter(experiment__name = experiment_name).delete()
    Marker.objects.filter(experiment__name = experiment_name).delete()
    Phenotype_Experiment.objects.filter(experiment__name = experiment_name).delete()
    Phenotype.objects.filter(phenotype_experiment=None).delete()
    
    Experiment.objects.get(name = experiment_name).delete()

def rename_experiment(old_name,new_name):
    old_experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (old_name))
    if not os.path.exists(old_experiment_dir):
        sys.stderr.write("Cannot rename experiment, directory does not exist: %s\n"%old_experiment_dir)
        return

    new_experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (new_name))
    if os.path.exists(new_experiment_dir):
        sys.stderr.write("Cannot rename experiment, new directory already exists: %s\n"%new_experiment_dir)
        return

    shutil.move(old_experiment_dir,new_experiment_dir)

    experiment = Experiment.objects.get(name=old_name)
    experiment.name = new_name
    experiment.save()

    create_trait_json_for_experiment(new_name)


def update_traits_for_all_experiments():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name",flat=True):
        print("updating traits for experiment: %s"%experiment_name)
        update_traits_for_experiment(experiment_name)
        

def update_traits_for_experiment(experiment_name):
    experiment = Experiment.objects.get(name=experiment_name)
    
    genes = Gene_Experiment.objects.filter(experiment__name = experiment_name)
    phenotypes = Phenotype_Experiment.objects.filter(experiment__name = experiment_name)
    traits = load_traitlist_for_experiment(experiment_name)
    
    gene_list = list()
    phenotype_list = list()
    
    traits = set(traits)
    
    for gene in genes:
        gene_list.append(gene.gene_id)
        if gene.gene_id not in traits:
            print("deleting gene: %s"%gene.gene_id)
            gene.delete()
            
    for phenotype in phenotypes:
        phenotype_list.append(phenotype.phenotype.phenotype_id)
        if phenotype.phenotype.phenotype_id not in traits:
            print("deleting phenotype: %s"%phenotype.phenotype.phenotype_id)
            phenotype.delete()
    
    gene_list = set(gene_list)
    phenotype_list = set(phenotype_list)
    
    for trait in traits:
        if trait not in gene_list and trait not in phenotype_list:
            genes = GeneInfo.objects.filter(gene_id=trait)
            if len(genes) == 1:
                gene = genes[0]
                print("adding gene: %s"%gene.gene_id)
                gene_experiment = Gene_Experiment()
                gene_experiment.gene_id = gene.gene_id
                gene_experiment.experiment = experiment
                gene_experiment.geneinfo = gene
                gene_experiment.save()
    
    remove_orphan_phenotypes()
    update_max_lod_for_experiment(experiment_name)
    
        
def remove_orphan_phenotypes():
    Phenotype.objects.filter(phenotype_experiment=None).delete()
    
    
def get_histogram(experiment_name, trait_list, threshold):
    lod_scores = np.absolute(load_lodscores_for_experiment(experiment_name))
    traits = load_traitlist_for_experiment(experiment_name)
    marker_list = load_markerlist_for_experiment(experiment_name)
    
    counts_per_marker = np.sum(lod_scores[np.isin(traits,trait_list)] >= threshold, axis=0)
    markers = Marker.objects.filter(experiment__name = experiment_name).values_list('name', 'chromosome__name', 'start') 
    marker_dict = {}
    for marker in markers:
        marker_dict[marker[0]] = marker[1:]
    
    histogram = {'markers':{}}
    
    for i, marker_name in enumerate(marker_list):
        marker = marker_dict[marker_name]
        histogram['markers'][marker_name] = {'chr': marker[0], 'start': marker[1], 'count': counts_per_marker[i]}
        
    return histogram

def get_trait_values_for_marker(experiment_name, marker, trait):
    pass
