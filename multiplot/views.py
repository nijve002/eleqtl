from django.shortcuts import render_to_response, HttpResponse, redirect
from django.conf import settings
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from main.models import Experiment, GeneInfo, Gene_Experiment, GeneGO, GO, Species, \
    Phenotype, Phenotype_Experiment

import suggestions
import main
from main import utils

import urllib2
import re

class trait:
     trait_id = ""
     type = ""
     description = ""
     name = ""
     chr = 0
     start = 0
     max_lod_score = 0
     
@csrf_exempt
def multiplot(request):
    '''
    select a trait and experiment
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == "GET":
        query = request.GET.get('query','')
        experiment_name = request.GET.get('experiment_name','all')
        
    elif request.method == "POST":
        query = request.POST.get('query','')
        experiment_name = request.POST.get('experiment_name','all')
    
    query = urllib2.unquote(query).strip().upper()
    query = re.sub(r',', r' ', query)
    query = re.sub(r'\s+', r' ', query)

    if not query:
        return render_to_response('multiplot.html', {'experiments': experiments})

    if experiment_name == "all":
        if re.match("GO:", query, re.I):
            # query is a GO term
            experiment_name = experiments[0].name
        else:
            return multiexperimentplot(query,experiment_name)

    trait_list = get_traits_for_query(experiment_name, query)

    if len(trait_list) == 0:
        ret = suggestions.views.index(request)
        if ret:
            return ret
        else:
            return main.views.no_results(query,"multiplot")

    title = query
    if re.match("GO:", query, re.I):
        title = "%s (%s)" % (GO.objects.filter(accession=query).values_list("name", flat=True)[0], query)
    elif len(trait_list) > 1:
        title = "%s traits"%len(trait_list)

    return render_to_response('multiplot.html', {'experiment_name': experiment_name,
                                                 'experiments': experiments,
                                                 'query': query,
                                                 'species': species_short_name,
                                                 'title': title,
                                                 'trait_list': sorted(trait_list,reverse=True,
                                                               key=lambda x: x.max_lod_score),
                                                 })


def multiexperimentplot(query,experiment_name):
    '''
    plot lod scores for one trait for all experiments
    
    '''
    
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    trait_list = list()
    
    query_fields = re.split('[\s;,+]+',query)
    
    genes = GeneInfo.objects.none()
    phenotypes = Phenotype.objects.none()
    
    # is it a gene
    for query_field in query_fields:
        genes |= GeneInfo.objects.filter(species__species_name=settings.SPECIES).\
            filter(Q(gene_id=query_field)|Q(gene_name__iexact=query_field)).distinct()
        phenotypes |= Phenotype.objects.\
            filter(Q(name__iexact=query_field)|Q(phenotype_id=query_field)).distinct()

    experiment_list = list()

    if genes.count() == 1:
        gene = genes[0]

        experiments4trait = Experiment.objects.filter(gene_experiment__gene_id=gene.gene_id).\
            values_list('name', flat=True)
        
        for experiment in experiments4trait:     
            t = trait()
            t.trait_id = gene.gene_id
            t.name = gene.gene_name
            t.start = gene.start
            t.chr = gene.chr
            t.description = gene.description
            t.max_lod_score = Gene_Experiment.objects.get(gene_id = gene.gene_id, experiment__name=experiment).max_lod_score
            t.type = "gene"
            t.experiment_name = experiment
            experiment_list.append(experiment)
            trait_list.append(t)

    elif phenotypes.count() > 0:      
        #phenotype = phenotypes[0]
        
        for phenotype in phenotypes:
            phenotype_experiments = Phenotype_Experiment.objects.filter(phenotype=phenotype)
                
            for phenotype_experiment in phenotype_experiments:
                experiment = phenotype_experiment.experiment.name
                experiment_list.append(experiment)
                t = trait()
                t.trait_id = phenotype.phenotype_id
                t.name = phenotype.name
                t.description = phenotype.description
                t.type = "phenotype"
                t.experiment_name = experiment
                t.max_lod_score = phenotype_experiment.max_lod_score
                trait_list.append(t)       
    else:
        ret = suggestions.views.suggest(query,experiment_name, None)
        if ret:
            return ret
        else:
            return main.views.no_results(query,"multiplot")
    
    if len(set(experiment_list)) == 1:
        experiment_name = experiment_list[0]

    title = query        

    return render_to_response('multiplot.html', {'trait_list': trait_list,
                                                 'experiments': experiments,
                                                 'title': title,
                                                 'query': query,
                                                 'experiment_name': experiment_name,
                                                 'experiments4trait': experiment_list,
                                                 'species': species_short_name})


def getGeneInfoForGeneIDs(geneIDs):
    geneInfoList = list()
    for geneID in geneIDs:
        if len(GeneInfo.objects.filter(gene_id=geneID)) == 1:
            geneInfoList.append(GeneInfo.objects.get(gene_id=geneID))

    return geneInfoList


def queryGenesForExperiment(experiment_name, query):
    geneIDs = query.split(" ")

    return Gene_Experiment.objects.filter(experiment__name=experiment_name).filter(gene_id__in=geneIDs).values_list(
        'gene_id', flat=True)


def queryGO(experiment_name, query):
    GO_ID = query

    genelist = list(GeneGO.objects.filter(term_accession=GO_ID).values_list('geneinfo__gene_id', flat=True))
    genelist = set(genelist)

    genelist = queryGenesForExperiment(experiment_name, ' '.join(genelist))
    return genelist

def get_traits_for_query(experiment_name,query):
    gene_list = list()
    phenotype_list = list()

    if re.match("GO:", query, re.I):
        # GO term
        gene_ids = queryGO(experiment_name, query)
        gene_list = getGeneInfoForGeneIDs(gene_ids)
    elif re.match("(chr)?(.+):(\d+)[.][.](\d+)",query,re.I):
        # chromosomal location
        mo = re.match("(chr)?(.+):(\d+)[.][.](\d+)",query,re.I)
        genes = GeneInfo.objects.filter(chr=mo.group(2)).filter(start__range=(mo.group(3),mo.group(4)))
        gene_list = genes.filter(gene_experiment__experiment__name = experiment_name)
    elif query == 'ALL':
        phenotype_list = Phenotype.objects.filter(phenotype_experiment__experiment__name = experiment_name)
    else:
        query_fields = re.split('[\s;,+]+',query)

        for q in query_fields:
            genes = GeneInfo.objects.filter(gene_experiment__experiment__name=experiment_name)\
                .filter(Q(gene_experiment__gene_id=q)|Q(gene_name=q)).distinct()
            if len(genes) == 1:
                gene_list.append(genes[0])

            phenotypes = Phenotype.objects.filter(Q(name__iexact=q)|Q(phenotype_id=q)).distinct()
            phenotypes = phenotypes.filter(phenotype_experiment__experiment__name = experiment_name)
            if len(phenotypes) > 0:
                phenotype_list.append(phenotypes[0])

    trait_list = list()
    
    gene_list = set(gene_list)
    for gene in gene_list:
        t = trait()
        t.trait_id = gene.gene_id
        t.type = "gene"
        t.description = gene.description
        t.name = gene.gene_name
        t.chr = gene.chr
        t.max_lod_score = Gene_Experiment.objects.get(gene_id = gene.gene_id, experiment__name=experiment_name).max_lod_score
        t.start = gene.start
        t.experiment_name = experiment_name
        trait_list.append(t)
        
    for phenotype in phenotype_list:
        t = trait()
        t.trait_id = phenotype.phenotype_id
        t.max_lod_score = phenotype.phenotype_experiment_set.get(experiment__name = experiment_name).max_lod_score
        t.name = phenotype.name
        t.type = "phenotype"
        t.experiment_name = experiment_name
        t.description = phenotype.description
        trait_list.append(t)

    return trait_list


def histogram(request):
    if request.method == "GET":
        query = request.GET.get('query')
        experiment_name = request.GET.get('experiment_name')
        
        trait_list = [trait.trait_id for trait in get_traits_for_query(experiment_name,query)]
        threshold = Experiment.objects.get(name=experiment_name).lodthld
        
        histogram = utils.get_histogram(experiment_name,trait_list,threshold)
        
        return JsonResponse(histogram)
        
def trait_values_for_marker(request):
    pass
