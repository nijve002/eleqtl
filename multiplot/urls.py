'''
Created on Jan 26, 2016

@author: harm nijveen
'''

from django.conf.urls import patterns, include, url
from multiplot.views import multiplot, histogram
from django.conf import settings
from django.conf.urls.static import static
 
urlpatterns = patterns('',
    url(r'^$', multiplot),
    url(r'^histogram', histogram),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
        
