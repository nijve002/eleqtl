import urllib2

from django.conf import settings
from django.shortcuts import render_to_response, HttpResponse

from main.models import GeneInfo, GO, GeneGO, Experiment, Phenotype

class GOTerm:
    accesssion = ""
    name = ""
    definition = ""
    domain = ""
    gene_count = 0

def index(request):
    if request.method == 'GET':

        if request.GET.get('query') and request.GET.get('experiment_name'):
            query = request.GET.get('query')
            query = urllib2.unquote(query).strip()
            experiment_name = request.GET.get('experiment_name')

            return suggest(query,experiment_name, None)

    return suggest(None,None,None)

def suggest(query, experiment_name, mode):
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)

    if not query:
        phenotypes = Phenotype.objects.all()
        return render_to_response('suggestions.html', {'Phenotypes': phenotypes,
                                                   'mode': mode,
                                                   'experiments': experiments,
                                                   'experiment_name': experiment_name})
                                                   
    if query == "FullGO":
        
        GO_terms = GO.objects.filter(genego__isnull=False).distinct()
            
        return render_to_response('suggestions.html', {
                                                'GOterms': GO_terms,
                                                'mode': mode,
                                                'experiments': experiments,
                                                'experiment_name': experiment_name})

    GO_terms = list(GO.objects.raw(
        "select * from main_go WHERE MATCH (name, definition) AGAINST ('%s' IN NATURAL LANGUAGE MODE)" % query))

    genes = list(GeneInfo.objects.raw(
        "select * from main_geneinfo WHERE MATCH (gene_name, description) AGAINST ('%s' IN NATURAL LANGUAGE MODE)" % query))

    gene_substring_results = GeneInfo.objects.filter(gene_name__icontains = query)

    for gene in gene_substring_results:
        genes.append(gene)

    genes = list(set(genes))

    phenotypes = list(Phenotype.objects.raw(
        "select * from main_phenotype WHERE MATCH (name, description,treatment,trait) AGAINST ('%s' IN NATURAL LANGUAGE MODE)" % query))

    if len(genes) == 0 and len(GO_terms) == 0 and len(phenotypes) == 0:
        return None

    return render_to_response('suggestions.html', {'Genes': genes,
                                                   'GOterms': GO_terms,
                                                   'Phenotypes': phenotypes,
                                                   'mode': mode,
                                                   'experiments': experiments,
                                                   'experiment_name': experiment_name})


