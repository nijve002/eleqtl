import os
import re

from django.shortcuts import render_to_response, HttpResponse
from django.conf import settings

import main
import suggestions
from main.models import Experiment, GeneInfo, Species, Marker
from main import utils

import numpy as np

# Create your views here.

class TraitMarker:
    type = ""
    description = ""
    trait_name = ""
    LOD = 0
    chr = 0
    start = 0
    end = 0


def selectMarker(request):
    '''
    select a marker, experiment and threshold
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == 'GET':
        if request.GET.get('experiment_name') and request.GET.get('query'):
            experiment_name = request.GET.get('experiment_name')
            query = request.GET.get('query').strip()

            if not query:
                return render_to_response('peakmarker.html', {'experiments': experiments})

            lodthld = Experiment.objects.get(name=experiment_name).lodthld

            if request.GET.get('thld'):
                try:
                    lodthld = float(request.GET.get('thld'))
                except ValueError:
                    return HttpResponse('<h1> invalid LOD threshold </h1>')

            min_dist = 0

            if request.GET.get('min_dist'):
                try:
                    min_dist = int(request.GET.get('min_dist'))
                except ValueError:
                    min_dist = 0

            marker_name = None

            if Marker.objects.filter(name=query, experiment__name=experiment_name):
                marker_name = Marker.objects.filter(name=query)[0].name
            elif Marker.objects.filter(name__iexact=query, experiment__name=experiment_name):
                markers = Marker.objects.filter(name__iexact=query)
                marker_name = markers[0].name
            elif Marker.objects.filter(name__iexact=query):
                markers = Marker.objects.filter(name__iexact=query)
                marker = markers[0]
                chromosome = marker.chromosome.name
                position = marker.start
                closestMarker = utils.get_closest_marker(experiment_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name
	    elif re.match("(chr)?(\w+):(\d+)$", query,re.I):
                genomicLocation = re.match("(chr)?(\w+):(\d+)$", query,re.I)
                chromosome = genomicLocation.group(2)
                position = genomicLocation.group(3)
                closestMarker = utils.get_closest_marker(experiment_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name
            elif GeneInfo.objects.filter(gene_id = query):
                geneInfo = GeneInfo.objects.get(gene_id = query)
                chromosome = geneInfo.chr
                position = geneInfo.start
                closestMarker = utils.get_closest_marker(experiment_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name

            if marker_name == None:
                ret = suggestions.views.suggest(query, experiment_name, "coregulation")
                if ret:
                    return ret
                else:
                    return main.views.no_results(query, "coregulation")

            lodscores = get_LODscores_for_marker(experiment_name, marker_name)
            traits = {t: l for (t, l) in lodscores.items() if l >= lodthld}
            
            # check that the marker is actually a peak for this gene
            (before_marker,after_marker) = utils.get_neighboring_markers(experiment_name, marker_name)
            if before_marker:
                before_lod = get_LODscores_for_marker(experiment_name, before_marker.name)
            if after_marker:
                after_lod = get_LODscores_for_marker(experiment_name, after_marker.name)
                
            # if the lod scores before or after the marker are higher, this is not a peak, 
            # so remove the gene from the list
            traits_to_remove = []
            for trait in traits:
                if (before_marker and (lodscores[trait] + 1.0) < before_lod[trait]) \
                    or (after_marker and (lodscores[trait] + 1.0) < after_lod[trait]):
                    traits_to_remove.append(trait)
                    
            for trait in traits_to_remove:
                del traits[trait]
                    

            marker = Marker.objects.get(name=marker_name,experiment__name=experiment_name)

            trait_list = list()
            for trait in traits:
                t = TraitMarker()
                t.trait_id = trait
                t.LOD = traits[trait]
                gi = GeneInfo.objects.filter(gene_id=trait)
                if gi:
                    t.description = gi[0].description
                    t.type = "gene"
                    t.name = gi[0].gene_name
                    t.chr = gi[0].chr
                    t.start = gi[0].start
                    t.end = gi[0].end
                    t.type = 'gene'
                    if min_dist and t.start:
                        dist = abs(long(t.start) - marker.start)/1000000
                        if t.chr == marker.chromosome.name and dist < min_dist:
                            continue
                else:
                    t.type = 'phenotype'

                trait_list.append(t)

            return render_to_response('peakmarker.html', {'experiment_name': experiment_name,
                                                          'experiments': experiments,
                                                          'species': species_short_name,
                                                          'marker': marker,
                                                          'min_dist': min_dist,
                                                          'trait_list': sorted(trait_list, key=lambda x: x.LOD,
                                                                              reverse=True),
                                                          'lodthld': lodthld})

        else:
            return render_to_response('peakmarker.html', {'experiments': experiments})


def get_LODscores_for_marker(experiment_name, marker_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    traits_pickle_path = os.path.join(experiment_data_path, '%s' % "traits.npy")
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")

    lod_scores = np.load(lod_pickle_path)
    traits = np.load(traits_pickle_path)
    markers = np.load(markers_pickle_path)

    mi = markers.tolist().index(marker_name)

    marker_lodscores = dict(zip(traits,lod_scores[:,mi]))

    return marker_lodscores
