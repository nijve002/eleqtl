'''
Created on Apr 29, 2015

@author: jiao
'''

from django.conf.urls import patterns, include, url
from coregulation.views import selectMarker
from django.conf import settings
from django.conf.urls.static import static
 
urlpatterns = patterns('',
    url(r'^', selectMarker),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
        
