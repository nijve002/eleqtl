-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: EleQTL
-- ------------------------------------------------------
-- Server version	5.7.21-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `GOGENE`
--

DROP TABLE IF EXISTS `GOGENE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GOGENE` (
  `gene_id` varchar(45) DEFAULT NULL,
  `GO_id` varchar(45) DEFAULT NULL,
  `domain` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` (`user_id`),
  KEY `djang_content_type_id_697914295151027a_fk_django_content_type_id` (`content_type_id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_chromosome`
--

DROP TABLE IF EXISTS `main_chromosome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_chromosome` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `species_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_main_chromosome_1_idx` (`species_id`),
  CONSTRAINT `fk_main_chromosome_1` FOREIGN KEY (`species_id`) REFERENCES `main_species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_experiment`
--

DROP TABLE IF EXISTS `main_experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_experiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `species_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phenotypes` varchar(50) NOT NULL,
  `type_of_array` varchar(50) NOT NULL,
  `sample_size` varchar(50) NOT NULL,
  `parental_strain` varchar(50) NOT NULL,
  `reference` varchar(200) NOT NULL,
  `pub_url` text NOT NULL,
  `pub_date` datetime NOT NULL,
  `upload_user_id` int(11) NOT NULL,
  `array_file` varchar(100) NOT NULL,
  `marker_file` varchar(100) NOT NULL,
  `genotype_file` varchar(100) NOT NULL,
  `lod_file` varchar(100) NOT NULL,
  `pvalthld` decimal(13,12) NOT NULL,
  `lodthld` decimal(6,3) NOT NULL,
  `sample` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_experiment_e1800d51` (`species_id`),
  KEY `upload_experiment_c5c1f9d1` (`upload_user_id`),
  CONSTRAINT `fk_main_experiment_species` FOREIGN KEY (`species_id`) REFERENCES `main_species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_gene_experiment`
--

DROP TABLE IF EXISTS `main_gene_experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_gene_experiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gene_id` varchar(50) NOT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  `geneinfo_id` int(11) DEFAULT NULL,
  `max_lod_score` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spot_id_idx` (`gene_id`),
  KEY `fk_main_gene_experiment_idx` (`experiment_id`),
  KEY `fk_main_gene_geneinfo_idx` (`geneinfo_id`),
  CONSTRAINT `fk_main_gene_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_main_gene_geneinfo` FOREIGN KEY (`geneinfo_id`) REFERENCES `main_geneinfo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=893165 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_gene_go`
--

DROP TABLE IF EXISTS `main_gene_go`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_gene_go` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gene_id` varchar(30) DEFAULT NULL,
  `term_accession` varchar(30) DEFAULT NULL,
  `term_evidence_code` varchar(30) DEFAULT NULL,
  `geneinfo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_gene_go_geneinfo_idx` (`gene_id`),
  KEY `fk_main_gene_go_geneinfo_idx1` (`geneinfo_id`),
  KEY `main_gene_go_ibfk_1` (`term_accession`),
  CONSTRAINT `fk_main_gene_go_geneinfo` FOREIGN KEY (`geneinfo_id`) REFERENCES `main_geneinfo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `main_gene_go_ibfk_1` FOREIGN KEY (`term_accession`) REFERENCES `main_go` (`accession`)
) ENGINE=InnoDB AUTO_INCREMENT=1874256 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_geneinfo`
--

DROP TABLE IF EXISTS `main_geneinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_geneinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gene_id` varchar(30) DEFAULT NULL,
  `gene_name` varchar(30) DEFAULT NULL,
  `description` text,
  `species_id` int(11) DEFAULT NULL,
  `chr` varchar(3) DEFAULT NULL,
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transcript_name_UNIQUE` (`gene_id`),
  KEY `fk_main_geneinfo_species_idx` (`species_id`),
  KEY `gene_id_idx` (`gene_id`),
  KEY `gene_name_idx` (`gene_name`) USING BTREE,
  FULLTEXT KEY `gene_name_fulltext` (`gene_name`,`description`),
  CONSTRAINT `fk_main_geneinfo_species` FOREIGN KEY (`species_id`) REFERENCES `main_species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81901 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_go`
--

DROP TABLE IF EXISTS `main_go`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_go` (
  `accession` varchar(30) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `definition` text,
  `domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`accession`),
  KEY `accession_idx` (`accession`),
  FULLTEXT KEY `fulltext_idx` (`name`,`definition`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_line`
--

DROP TABLE IF EXISTS `main_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line_name` varchar(50) NOT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_line_experiment_idx` (`experiment_id`),
  CONSTRAINT `fk_main_line_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1339 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_marker`
--

DROP TABLE IF EXISTS `main_marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_marker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  `chromosome_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_marker_experiment_idx` (`experiment_id`),
  KEY `fk_main_marker_chromosome_idx` (`chromosome_id`),
  CONSTRAINT `fk_main_marker_chromosome` FOREIGN KEY (`chromosome_id`) REFERENCES `main_chromosome` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_main_marker_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=109935 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_marker_experiment`
--

DROP TABLE IF EXISTS `main_marker_experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_marker_experiment` (
  `id` int(11) NOT NULL,
  `marker_id` int(11) NOT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_marker_experiment_1_idx` (`experiment_id`),
  KEY `fk_main_marker_experiment_2_idx` (`marker_id`),
  CONSTRAINT `fk_main_marker_experiment_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_main_marker_experiment_marker` FOREIGN KEY (`marker_id`) REFERENCES `main_marker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_metabolite`
--

DROP TABLE IF EXISTS `main_metabolite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_metabolite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `metabolite_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `metabolite_id_UNIQUE` (`metabolite_id`),
  KEY `name_index` (`name`),
  FULLTEXT KEY `name_description_full_index` (`name`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_metabolite_experiment`
--

DROP TABLE IF EXISTS `main_metabolite_experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_metabolite_experiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `experiment_id` int(11) DEFAULT NULL,
  `metabolite_id` int(11) DEFAULT NULL,
  `max_lod_score` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_metabolite_experiment_1_idx` (`metabolite_id`),
  KEY `fk_main_metabolite_experiment_2_idx` (`experiment_id`),
  CONSTRAINT `fk_main_metabolite_experiment_1` FOREIGN KEY (`metabolite_id`) REFERENCES `main_metabolite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_main_metabolite_experiment_2` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_phenotype`
--

DROP TABLE IF EXISTS `main_phenotype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_phenotype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `phenotype_id` varchar(255) NOT NULL,
  `phenotype` text,
  `treatment` text,
  `trait` text,
  `statistic` text,
  `unit` text,
  `comment` text,
  `stage` text,
  `GO_term` text,
  `population` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phenotype_id_UNIQUE` (`phenotype_id`),
  KEY `name_index` (`name`),
  FULLTEXT KEY `keyword_search` (`phenotype`,`treatment`,`trait`,`statistic`,`unit`,`comment`,`stage`,`GO_term`,`population`),
  FULLTEXT KEY `name_description_full_index` (`name`,`description`,`treatment`,`trait`)
) ENGINE=InnoDB AUTO_INCREMENT=2973 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_phenotype_experiment`
--

DROP TABLE IF EXISTS `main_phenotype_experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_phenotype_experiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `experiment_id` int(11) DEFAULT NULL,
  `phenotype_id` int(11) DEFAULT NULL,
  `max_lod_score` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_metabolite_experiment_1_idx` (`phenotype_id`),
  KEY `fk_main_metabolite_experiment_2_idx` (`experiment_id`),
  CONSTRAINT `fk_main_phenotype_experiment_1` FOREIGN KEY (`phenotype_id`) REFERENCES `main_phenotype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_main_phenotype_experiment_2` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3627 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_species`
--

DROP TABLE IF EXISTS `main_species`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_species` (
  `species_name` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `URL` varchar(255) DEFAULT NULL,
  `short_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_transcript`
--

DROP TABLE IF EXISTS `main_transcript`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_transcript` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transcript_id` varchar(20) DEFAULT NULL,
  `chr` varchar(3) NOT NULL,
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `geneinfo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_transcript_geneinfo_idx` (`geneinfo_id`),
  KEY `transcript_id` (`transcript_id`),
  CONSTRAINT `fk_main_transcript_geneinfo` FOREIGN KEY (`geneinfo_id`) REFERENCES `main_geneinfo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=263642 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-12 15:39:37
