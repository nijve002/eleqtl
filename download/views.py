from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import re

@csrf_exempt
def index(request):
    download_data = ""
    if request.method == 'GET':
        if request.GET.get('data'):
            download_data = request.GET.get('data');
            download_data = re.sub(",","\n",download_data)
    elif request.method == 'POST':
        if request.POST.get('data'):
            download_data = request.POST.get('data');
            download_data = re.sub(",","\n",download_data)

    return HttpResponse(download_data, content_type='text/plain')



